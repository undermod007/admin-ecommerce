<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\ProdukControlller;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * route "/register"
 * @method "POST"
 */
Route::post('/register', App\Http\Controllers\Api\RegisterController::class)->name('register');

/**
 * route "/login"
 * @method "POST"
 */
Route::post('/login', App\Http\Controllers\Api\LoginController::class)->name('login');

/**
 * route "/logout"
 * @method "POST"
 */
Route::post('/logout', App\Http\Controllers\Api\LogoutController::class)->name('logout');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['auth:api'])->group(function() {

    // Produk

    /**
     * route "/produk/getData"
     * @method "GET"
     */
    Route::get('/produk/getData', [ProdukControlller::class, 'getData'])->name('produk.getData');

    /**
     * route "/produk/store"
     * @method "POST"
     */
    Route::post('/produk/store', [ProdukControlller::class, 'store'])->name('produk.store');

    /**
     * route "/produk/edit/{id}"
     * @method "GET"
     */
    Route::get('/produk/edit/{id}', [ProdukControlller::class, 'edit'])->name('produk.edit');

    /**
     * route "/produk/update/{id}"
     * @method "GET"
     */
    Route::post('/produk/update/{id}', [ProdukControlller::class, 'update'])->name('produk.update');

    /**
     * route "/produk/destroy/{id}"
     * @method "DELETE"
     */
    Route::delete('/produk/destroy/{id}', [ProdukControlller::class, 'destroy'])->name('produk.destroy');
});

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// Import Model User
use App\Models\User;
// Import Facades Validator
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // Set validasi untuk keamanan submit form registrasi
        $validator = Validator::make($request->all(), [
            'name' => 'required', 
            // required = kolom input tidak boleh kosong
            'email' => 'required|email|unique:users',
            // required = kolom input tidak boleh kosong
            // email = tipe input harus berupa email
            // unique = email users tidak boleh sama (email hanya bisa di gunakan di satu akun)
            'password' => 'required|min:8|confirmed'
            // required = kolom input tidak boleh kosong
            // min:8 = pajang karakter yang diinput minimal 8
            // confirmed = input harus mempunyai inputan lagi untuk konfirmasi data yang sama.
        ]);

        // Jika validator gagal maka laravel akan me return $validator error berupa response json
        if($validator->fails())  {
            return response()->json(['errors' => $validator->errors()]);
        }

        // Jika validator berhasil laravel akan menjalankan fungsi create()
        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password'))
            // bcrypt() di gunakan untuk mengenkripsi string password untuk alasan keamanan autentikasi
        ]);

        // Jika user baru telah di tambah kan laravel akan me return $user berupa response json
        if($user) {
            return response()->json([
                'success' => true,
                // pesan success
                'user' => $user
                // me return data user yang di tambah
            ], 201);
        }

        // Jika fungsi create() gagal di jalankan laravel akan me return error berupa response json
        return response()->json([
            'success' => false,
            // pesan success
        ], 409);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// Import Model User
use App\Models\User;
// Import Facades Validator
use Illuminate\Support\Facades\Validator;
// Import Inteface JWTAuth
use Tymon\JWTAuth\Facades\JWTAuth;


class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        $credential = $request->only('email', 'password');

        // Jika autentikasi gagal
        if(!$token = JWTAuth::attempt($credential)) {
            return response()->json(['message' => 'Email atau Password salah']);
        }

        // Jika autentikasi berhasil
        return response()->json([
            'success' => true,
            'user' => auth()->user(),
            'token' => $token
        ], 200);
    }
}

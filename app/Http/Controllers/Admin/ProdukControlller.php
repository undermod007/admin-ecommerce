<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// Import Model Produk
use App\Models\Admin\Produk;
use Illuminate\Support\Facades\Validator;

class ProdukControlller extends Controller
{
    // Fungsi getData digunakan untuk mengambil data dari tabel produk 

    public function getData()
    {
        $produk = Produk::paginate(10);

        // Jika data produk berhasil diambil
        if($produk) {
            return response()->json($produk);
        }

        // Jika data produk gagal diambil
        return response()->json([
            'success' => false
        ], 409);
    }

    // Fungsi store digunakan untuk memasukan data baru ke dalam database

    public function store(Request $request)
    {
        // Set validasi untuk submit form
        $validator = Validator::make($request->all(), [
            'nama_produk' => 'required',
            'deskripsi' => 'required|min:100',
            'harga' => 'required',
            'gambar' => 'required'
        ]);

        // Jika validasi gagal dilakukan
        if($validator->fails()) {
            // return error validasi
            return response()->json(['errors' => $validator->errors()]);
        }

        // Jika validasi berhasil dilakukan
        $file = $request->file('gambar');
        $namaFile = $file->getClientOriginalName();
        $file->move(public_path().'/admin/produk/gambar', $namaFile);

        $produk = Produk::create([
            'nama_produk' => $request->input('nama_produk'),
            'deskripsi' => $request->input('deskripsi'),
            'harga' => $request->input('harga'),
            'gambar' => $namaFile
        ]);

        // Jika data berhasil masuk ke database
        if($produk) {
            return response()->json([
                'success' => true,
                'data' => $produk
            ], 201);
        }

        // Jika data gagal masuk ke database
        return response()->json([
            'success' => false
        ], 409);
    }

    // Fungsi edit di gunakan untuk menampilakan data yang akan di edit, berdasarkan id yang di request
    
    public function edit(Request $request)
    {
        // Jika fungsi edit di jalankan maka laravel kan mencari produk yang akan di edit dari database
        $produk = Produk::find($request->id);

        // Jika data dengan id yang di minta di temukan maka selanjutnya kita akan tampilkan data tersebut
        // dalam bentuk input yang siap di edit

        $html = "<div class='card-body'>
                    <div class='form-group'>
                        <input type='hidden' class='form-control' id='id' value='".$produk->id."' name='id'>
                    </div>
                    <div class='form-group'>
                        <label for='nama_produk_edit'>Nama Produk</label>
                        <input type='text' class='form-control' id='nama_produk_edit' value='".$produk->nama_produk."' name='nama_produk'>
                    </div>
                    <div class='form-group'>
                        <label for='deskripsi_edit'>Deskripsi</label>
                        <textarea name='deskripsi' id='deskripsi_edit' class='form-control'>".$produk->deskripsi."</textarea>
                    </div>
                    <div class='form-group'>
                        <label for='harga_edit'>Harga</label>
                        <input type='number' name='harga' id='harga_edit' value='".$produk->harga."' class='form-control'>
                    </div>
                    <div class='form-group'>
                        <label for='gambar_edit'>Gambar</label>
                            <div class='input-group'>
                                <div class='custom-file'>
                                    <input type='file' class='form-control' id='gambar_edit' name='gambar'>
                                    <label class='custom-file-label' for='gambar_edit'>".$produk->gambar."</label>
                                </div>
                                <div class='input-group-append'>
                                    <span class='input-group-text'>Upload</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           ";

            return response()->json(['html' => $html]);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_produk' => 'required',
            'deskripsi' => 'required|min:100',
            'harga' => 'required',
            'gambar' => 'required'
        ]);

        if($validator->fails()) {
            // return error validasi
            return response()->json(['errors' => $validator->errors()]);
        }

        $produk = Produk::find($request->id);
        if($request->file('gambar') != null && $request->hasFile('gambar'))
        {
            $file = 'admin/produk/gambar/'. $produk->gambar;
            if(is_file($file))
            {
                unlink($file);
            }
            $file = $request->file('gambar');
            $fileName = $file->getClientOriginalName();
            $request->file('gambar')->move('admin/produk/gambar/', $fileName);
            $produk->gambar = $fileName;
        }
        $produk->nama_produk = $request->input('nama_produk');
        $produk->deskripsi = $request->input('deskripsi');
        $produk->harga = $request->input('harga');
        $produk->gambar = $fileName;
        $produk->save();

        if($produk) {
            return response()->json([
                'success' => true,
                'produk' => $produk
            ], 201);
        }

        return response()->json([
            'success' => false
        ], 409);
    }

    public function destroy(Request $request)
    {
        $produk = Produk::find($request->id);
        $file = 'admin/produk/gambar/'. $produk->gambar;

        if(is_file($file)) {
            unlink($file);
        }

        $produk->delete();

        if($produk) {
            return response()->json([
                'success' => true,
                'produk' => $produk
            ], 201);
        }

        return response()->json([
            'success' => false
        ], 409);
    }
}

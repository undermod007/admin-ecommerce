@extends('layouts.admin', ['title' => 'Produk'])

@section('style')
    <!-- Import Plugin Style Bootstrap DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('breadcrumb')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Produk</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">Produk</li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-11">
            <div class="card shadow">
                <div class="card-body">
                    Crud Produk
                </div>
            </div>
        </div>
        <div class="col-md-1">
            {{-- Tombol Triger #ModalTambah --}}
            <button class="btn btn-primary btn-lg" style="border-radius: 50%; height: 4rem; width: 4rem;" data-toggle="modal" data-target="#ModalTambah">
                <i class="fas fa-plus"></i>
            </button>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-body">
                    <table class="table table-bordered" id="tabel-produk">
                        <thead>
                            <tr>
                                <th>Nama Produk</th>
                                <th>Deskripsi</th>
                                <th>Harga</th>
                                <th>Gambar</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Tambah --}}

    <div class="modal fade" id="ModalTambah" data-backdrop="static" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nama_produk">Nama Produk</label>
                            {{-- Input Nama Produk --}}
                            <input type="text" class="form-control" id="nama_produk" name="nama_produk">
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Deskripsi</label>
                            {{-- Input Deskripsi --}}
                            <textarea name="deskripsi" id="deskripsi" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="harga">Harga</label>
                            {{-- Input Harga --}}
                            <input type="number" name="harga" id="harga" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="gambar">Gambar</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        {{-- Input Gambar --}}
                                        <input type="file" class="custom-file-input" id="gambar" name="gambar">
                                        <label class="custom-file-label" for="gambar">Choose file</label>
                                    </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Upload</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-tambah">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Edit --}}

    <div class="modal fade" id="ModalEdit" data-backdrop="static" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" id="form-edit">
                    {{-- Form Edit akan di render di sini --}}
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-update">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <!-- Import Javascript DataTables & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <!-- bs-custom-file-input -->
    <script src="{{ asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script>
        // Ketika halaman produk berhasil di load / refresh maka java script akan menjalankan script di bawah ini

        $(document).ready(function() {

            // Mengambil Token JWT
            let token = localStorage.getItem('token');
            
            // Mengaktifkan DataTables dengan Javascript DOM dan JQuery

            $('#tabel-produk').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                // Mengambil data produk menggunakan ajax melalui api
                ajax: {
                    url: `http://admin-ecommerce.test/api/produk/getData`,
                    type: 'GET',
                    // Memasukan Token JWT memalui headers
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                },
                // Generate Columns DataTable
                "columns": [
                    // Menampilkan data nama produk
                    {"data": "nama_produk"},
                    // Menampilkan data deskripsi
                    {"data": "deskripsi"},
                    // Menampilkan data harga
                    {"data": "harga"},
                    // Menampilkan data gambar
                    {"data": "gambar",
                        // Merender Tag img
                        "render": function(gambar) {
                            return `<img class="img-fluid" src="http://admin-ecommerce.test/admin/produk/gambar/${gambar}" width="200px">`
                        }
                    },
                    // Menampilkan tombol edit dan hapus
                    {"data": "id",
                        "render": function(id) {
                            // Edit dan Hapus berdasarkan id
                            return `
                                    <button class="btn btn-warning btn-edit" data-id-edit="${id}">Edit</button>
                                    |
                                    <button class="btn btn-danger btn-hapus" data-id-hapus="${id}">Hapus</button>
                                    `
                        }
                    }
                ]
            })

            $(function () {
                bsCustomFileInput.init();
            });

            // Javascript dom jika tombol dengan class btn-tambah di klil
            $('.btn-tambah').click(function(e) {
                // Mengubah agar tombol menjadi default
                e.preventDefault()

                // Input Nama Produk
                var nama_produk = $('#nama_produk').val()
                // Input Deskripsi
                var deskripsi = $('#deskripsi').val()
                // Input Harga
                var harga = $('#harga').val()
                // Input Gambar
                var gambar = $('#gambar')[0].files[0]

                // FormData yang akan disubmit dengan ajax
                var formData = new FormData()
                formData.append('nama_produk', nama_produk)
                formData.append('deskripsi', deskripsi)
                formData.append('harga', harga)
                formData.append('gambar', gambar)

                $.ajax({
                    // url api untuk fungsi store
                    url: `http://admin-ecommerce.test/api/produk/store`,
                    type: 'POST',
                    // memasukan token jwt melalui headers
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                    // mengirim data ke database
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    // jika data berhasil dimasukan
                    success: function(result) {
                        if(result.errors) {
                            // jika terjadi error
                            Swal.fire({
                                icon: 'error',
                                title: 'Validation Errors',
                                // menampilkan pesan error validasi
                                html: '<div class="text-danger errors">'+result.errors.nama_produk+'</div>' +
                                    '<div class="text-danger errors">'+result.errors.deskripsi+'</div>' +
                                    '<div class="text-danger errors">'+result.errors.harga+'</div>'
                                ,
                            })
                            // console.log(result.errors.nama_produk)
                            // jika tidak terjadi error
                        } else {
                            // Pesan suskes akan ditampilkan
                            Swal.fire({
                                icon: 'success',
                                title: 'Data Added Succsessfully',
                                showConfirmButton: true
                            })
                            // Jika tombok ok di klik
                            .then((result) => {
                                // Modal tambah akan di tutup
                                $('#ModalTambah').modal('hide')
                                // lalu tabel produk akan mengambil data dari data base secara otomatis
                                $('#tabel-produk').DataTable().ajax.reload()
                            })
                        }
                    }
                })
            })

            // Jika tombol dengan class btn-warning di klik
            $('body').on('click', '.btn-edit', function(e) {
                e.preventDefault()

                // Mengambil id dari atribut data-id-edit

                var id = $(this).attr('data-id-edit')

                $.ajax({
                    url: `http://admin-ecommerce.test/api/produk/edit/${id}`,
                    type: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                    // Jika berhasil
                    success: function(result) {

                        // Modal Edit akan di munculkan

                        $('#ModalEdit').modal('show')

                        // Form Edit akan di render

                        $('#form-edit').html(result.html)
                    }
                })
            })

            // Jika tombol dengan class btn-update di klik

            $('.btn-update').click(function(e) {
                e.preventDefault()

                // Input Nama Produk
                var id = $('#id').val()
                // Input Nama Produk
                var nama_produk = $('#nama_produk_edit').val()
                // Input Deskripsi
                var deskripsi = $('#deskripsi_edit').val()
                // Input Harga
                var harga = $('#harga_edit').val()
                // Input Gambar
                var gambar = $('#gambar_edit')[0].files[0]

                // FormData yang akan disubmit dengan ajax
                var formData = new FormData()
                formData.append('nama_produk', nama_produk)
                formData.append('deskripsi', deskripsi)
                formData.append('harga', harga)
                formData.append('gambar', gambar)

                $.ajax({
                    // url api untuk fungsi store
                    url: `http://admin-ecommerce.test/api/produk/update/${id}`,
                    type: 'POST',
                    // memasukan token jwt melalui headers
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                    // mengirim data ke database
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    // jika data berhasil dimasukan
                    success: function(result) {
                        if(result.errors) {
                            // jika terjadi error
                            Swal.fire({
                                icon: 'error',
                                title: 'Validation Errors',
                                // menampilkan pesan error validasi
                                html: '<div class="text-danger errors">'+result.errors.nama_produk+'</div>' +
                                    '<div class="text-danger errors">'+result.errors.deskripsi+'</div>' +
                                    '<div class="text-danger errors">'+result.errors.harga+'</div>'
                                ,
                            })
                            // console.log(result.errors.nama_produk)
                            // jika tidak terjadi error
                        } else {
                            // Pesan suskes akan ditampilkan
                            Swal.fire({
                                icon: 'success',
                                title: 'Data Added Succsessfully',
                                showConfirmButton: true
                            })
                            // Jika tombok ok di klik
                            .then((result) => {
                                // Modal Edit akan di tutup
                                $('#ModalEdit').modal('hide')
                                // lalu tabel produk akan mengambil data dari data base secara otomatis
                                $('#tabel-produk').DataTable().ajax.reload()
                            })
                        }
                    }
                })
            })

            $('body').on('click', '.btn-hapus', function(e) {
                e.preventDefault();

                var id = $(this).attr('data-id-hapus');

                $.ajax({
                    url: `http://admin-ecommerce.test/api/produk/destroy/${id}`,
                    type: 'DELETE',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                    success: function(result) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Data Deleted Successfully',
                            showConfirmButton: true
                        }).then((result) => {
                            if(result.isConfirmed) {
                                $('#tabel-produk').DataTable().ajax.reload()
                            }
                        })
                    }
                })
            })
        });
    </script>
@endsection
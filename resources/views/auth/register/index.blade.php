@extends('layouts.auth', ['title' => 'Register'])

@section('content')
<div class="register-box">
    <div class="card card-outline card-primary">
      <div class="card-header text-center">
        <a href="" class="h1"><b>Admin</b>LTE</a>
      </div>
      <div class="card-body">
        <p class="login-box-msg">Register a new membership</p>

        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
  
        <form id="form-register">
          <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="Name" id="name" name="name">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="email" class="form-control" placeholder="Email" id="email" name="email">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="password" class="form-control" placeholder="Password" id="password" name="password">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="password" class="form-control" placeholder="Retype password" id="password_confirmation" name="password_confirmation">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <button class="btn btn-primary btn-block btn-register">Register</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
  
        <div class="mt-1">
            <a href="{{ url('/') }}" class="text-center">I already have a membership</a>
        </div>
      </div>
      <!-- /.form-box -->
    </div><!-- /.card -->
</div>
<!-- /.register-box -->
@endsection

@section('script')
<script type="text/javascript">

    // Mengambil Token JWT
    const token = localStorage.getItem('token')
    // Jika Token Berhasil Diambil
    if(token) {
      location.replace(`http://admin-ecommerce.test/dashboard`)
    }

    // Jika halaman register berhasil di load / refresh maka javascript akan menjalankan script di bawah
    $(document).ready(function() {
        $('.btn-register').click(function (e) {
			e.preventDefault()

			var name = $('#name').val();
            var email = $('#email').val();
            var password = $('#password').val();
            var password_confirmation = $('#password_confirmation').val();

			$.ajax({
				url: `http://admin-ecommerce.test/api/register`,
				type: 'POST',
				data: {
					'name': name,
					'email': email,
					'password': password,
					'password_confirmation': password_confirmation
				},
				success: function (result) {
					if(result.errors) {
						Swal.fire({
							icon: 'error',
							title: 'Validation Errors',
							html: 	'<div class="text-danger errors">'+result.errors.name+'</div>' + 
									'<div class="text-danger errors">'+result.errors.email+'</div>' +
									'<div class="text-danger errors">'+result.errors.password+'</div>'
						})
					} else {
						Swal.fire({
							icon: 'success',
							title: 'Registration Successfully',
							text: 'Login now',
							showCancelButton: false,
							showCloseButton: false,
							showConfirmButton: true,
							showLoaderOnConfirm: true,
						}).then((result) => {
							if(result.isConfirmed) {
							location.replace(`http://admin-ecommerce.test/`);
							}
						})
					}
				}
			})

            // axios.post(`http://admin-ecommerce.test/api/register`, formData)
            // .then((response) => {
            //   Swal.fire({
            //     icon: 'success',
            //     title: 'Registration Successfully',
            //     text: 'Login now',
            //     showCancelButton: false,
            //     showCloseButton: false,
            //     showConfirmButton: true,
            //     showLoaderOnConfirm: true,
            //   }).then((result) => {
            //     if(result.isConfirmed) {
            //       location.replace(`http://admin-ecommerce.test/login`);
            //     }
            //   })
            // })
        })
    });
</script>
@endsection
@extends('layouts.auth', ['title' => 'Login'])

@section('content')
<div class="login-box">
    <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="" class="h1"><b>Admin</b>LTE</a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form id="form-login">
        <div class="input-group mb-3">
          <input type="email" class="form-control" placeholder="Email" id="email" name="email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" id="password" name="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-12">
            <button class="btn btn-primary btn-block">Login</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mb-1 mt-1">
        <a href="">I forgot my password</a>
      </p>
      <p class="mb-0">
        <a href="{{ url('/register') }}" class="text-center">Register a new membership</a>
      </p>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->
@endsection

@section('script')
<script type="text/javascript">
    let token = localStorage.getItem('token')
    if(token) {
      location.replace(`http://admin-ecommerce.test/dashboard`)
    }

    $(document).ready(function() {
        $('.btn-primary').click(function(e) {
            e.preventDefault()

            var email = $('#email').val()
            var password = $('#password').val()

            var formData = new FormData();
            formData.append('email', email);
            formData.append('password', password);

            $.ajax({
                url: `http://admin-ecommerce.test/api/login`,
                type: 'POST',
                data: {
                    'email': email,
                    'password': password
                },
                success: function (result) {
                    if(result.errors) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Validation Error',
                            html: '<div class="text-danger errors">'+result.errors.email+'</div>' + '<div class="text-danger errors">'+result.errors.password+'</div>',
                        })
                    } else if(result.message) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Authentication Error',
                            html: '<div class="text-danger errors">'+result.message+'</div>',
                            showConfirmButton: true
                        })
                    } else {
                        localStorage.setItem('token', result.token)
                        // console.log(result.token)
                        location.replace(`http://admin-ecommerce.test/dashboard`)
                    }
                }
            })
        })
    });
</script>
@endsection